/usr/bin/node $HOME/testing/css_tests/run_and_report_and_screenshot.js
cd $GIT_SCREENSHOT_REPO
if [ $# -eq 0 ];then
  NEW_DIR_NAME="screenshots_$(date +%Y-%m-%d-%T)_$(git branch | grep \* | cut -d ' ' -f2)_$(git rev-parse --verify HEAD)"
else
  NEW_DIR_NAME=$1
fi
mkdir $NEW_DIR_NAME
mv ./*.jpeg $NEW_DIR_NAME
/usr/local/bin/git add .
/usr/local/bin/git commit -m 'new screenshots'
/usr/local/bin/git push origin master 
