const axios = require('axios');
const bluebird = require('bluebird');

const config = {
 auth: {
  username: process.env.BROWSERSTACK_USER,
  password: process.env.BROWSERSTACK_KEY 
 }
}

axios.get('https://www.browserstack.com/automate/builds.json?limit=1', config)
.then((response) => {
  const buildID = response.data[0].automation_build.hashed_id;
  const url = 'https://www.browserstack.com/automate/builds/<build-id>/sessions.json?limit=1'.replace('<build-id>', buildID);
  return axios.get(url, config);
})
.then((response) => {
  const publicURL = response.data[0].automation_session.public_url;
  const slackURL = process.env.SLACK_BROWSERSTACK_HOOK;
// post to slack
//  return axios.post(slackURL, {text: 'A test has been run on BrowserStack. Check out the results at ' + publicURL}, config);
})
.catch((err) => {
 console.log('err', err)
});

