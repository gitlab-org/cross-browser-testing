#!/bin/sh

if [ $# -eq 0 ];then
  BRANCH_TO_CHECK="master"
else
  BRANCH_TO_CHECK=$1
fi

sudo service gitlab stop
sudo service nginx stop
cd /home/git/gitlab

sudo -u postgres /usr/bin/psql -d template1 -c "DROP DATABASE gitlabhq_production";
sudo -u postgres /usr/bin/psql -d template1 -c "CREATE DATABASE gitlabhq_production OWNER git;"

/usr/local/bin/git checkout -f
/usr/local/bin/git checkout $BRANCH_TO_CHECK
/usr/local/bin/git fetch --all
/usr/local/bin/git checkout -- Gemfile.lock db/schema

if [ $# -eq 0 ];then
  /usr/local/bin/git reset --hard origin/master
fi

/usr/local/bin/bundle install --without development test mysql --no-deployment
/usr/local/bin/bundle clean --force
/usr/local/bin/bundle exec rake db:migrate RAILS_ENV=production
/usr/local/bin/bundle exec rake yarn:install gitlab:assets:clean gitlab:assets:compile cache:clear RAILS_ENV=production NODE_ENV=production
/usr/local/bin/bundle exec rake "gitlab:workhorse:install[/home/git/gitlab-workhorse]" RAILS_ENV=production
cd /home/git/gitlab-shell
/usr/local/bin/git fetch --all --tags
/usr/local/bin/git checkout v`cat /home/git/gitlab/GITLAB_SHELL_VERSION`
/usr/local/bin/bundle install
sh -c 'if [ -x bin/compile ]; then bin/compile; fi'
cd /home/git/gitlab
sudo -u git -H yes "yes" | bundle exec rake gitlab:setup RAILS_ENV=production

sudo service gitlab start
sudo service nginx restart

