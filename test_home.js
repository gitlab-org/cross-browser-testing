const loginHelpers = require('./user_helpers');

module.exports = {
  
  'GitLab New Project' : function (browser) {
    loginHelpers.login(browser);
    browser
      .waitForElementPresent('.blank-state',5000)
      .click('.blank-state .btn.btn-new[href="/projects/new"]')
      .waitForElementPresent('.profile-settings-sidebar h4',5000)
      .assert.containsText('.profile-settings-sidebar h4', 'New project')
      .end();
       
  }
};
