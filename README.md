# Cross Browser Testing [WIP]

Run Cross Browser testing against GitLab with Diffing of images. A WIP

* backup.tar - a db backup of GitLab at a nice restore point to dump the database. Can be configured to be created on nightly update.

* kill_restore_db.sh - script that wipes the database and creates a fresh start to start testing again. 

* run_and_report.sh & .js - together they run the tests against browserstack.

* run_and_report_and_screenshot.sh & js - together they do the same report as run_and_report but they also do some hackyness to grab the screenshots from browserstack, and upload them to a git repo of your choice.

* test_home - the main tests to run with nightwatch. 

* update_gitlab.sh - a nuclear option to update gitlab nightly. Add to cronjobs to update gitlab against master. Sometimes it won't work when master is broken, but no worries, it will just try again the next time. Can take 1 optional argument e.g. `tags/v9.2.7` for checking out a tag. 