#!/bin/sh

sudo service gitlab stop
sudo service nginx stop
sudo -u postgres /usr/bin/psql -d template1 -c "DROP DATABASE gitlabhq_production";
sudo -u postgres /usr/bin/psql -d template1 -c "CREATE DATABASE gitlabhq_production OWNER git;"
sudo -u postgres /usr/bin/pg_restore -d gitlabhq_production /home/git/backup/backupfile.tar
sudo service gitlab start
sudo service nginx start
