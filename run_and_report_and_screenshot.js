require('longjohn');

const axios = require('axios');
const bluebird = require('bluebird');
const fs = require('fs');
const https = require('https');
const async = require('async');
const NodeGit = require('nodegit');
const path = require('path');

const config = {
 auth: {
  username: process.env.BROWSERSTACK_USER,
  password: process.env.BROWSERSTACK_KEY 
 }
}

var download = function(url, dest, cb) {
  var file = fs.createWriteStream(dest);
  var request = https.get(url, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close();
      cb();
    });
  });
}

axios.get('https://www.browserstack.com/automate/builds.json?limit=1', config)
.then((response) => {
  const buildID = response.data[0].automation_build.hashed_id;
  const url = 'https://www.browserstack.com/automate/builds/<build-id>/sessions.json?limit=1'.replace('<build-id>', buildID);
  return axios.get(url, config);
})
.then((response) => {
  let url = response.data[0].automation_session.logs;
  url = url + '.json';
  return axios.get(url, config);
})
.then((response) => {
  // scrap from browserstack
  const regex = /((http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)\w*.jpeg\b/g;
  const pwd = `${process.env.HOME}/testing/screenshot-archive/`;
  let i = 1, threads = 5, files = [], index, oid, repo;
  
  async.eachLimit(response.data.match(regex), threads, (url, next) => {
    const filePath =  pwd + 'screenshot_' + new Date().toISOString() + "_"  + (i++) + ".jpeg";
    files.push(filePath);
    download(url, filePath , next);
  }, () => {
   console.log('done downloading files');
  });  
})
.catch((err) => {
 console.log('err', err)
});

